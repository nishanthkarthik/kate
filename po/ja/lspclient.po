msgid ""
msgstr ""
"Project-Id-Version: kate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-25 00:49+0000\n"
"PO-Revision-Date: 2019-06-30 16:17-0700\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: gotosymboldialog.cpp:157 lspclientsymbolview.cpp:252
#, kde-format
msgid "Filter..."
msgstr ""

#. i18n: ectx: Menu (LSPClient Menubar)
#: lspclientconfigpage.cpp:98 lspclientconfigpage.cpp:103
#: lspclientpluginview.cpp:483 lspclientpluginview.cpp:640 ui.rc:6
#, kde-format
msgid "LSP Client"
msgstr ""

#: lspclientconfigpage.cpp:213
#, kde-format
msgid "No JSON data to validate."
msgstr ""

#: lspclientconfigpage.cpp:222
#, kde-format
msgid "JSON data is valid."
msgstr ""

#: lspclientconfigpage.cpp:224
#, kde-format
msgid "JSON data is invalid: no JSON object"
msgstr ""

#: lspclientconfigpage.cpp:227
#, kde-format
msgid "JSON data is invalid: %1"
msgstr ""

#: lspclientconfigpage.cpp:275
#, kde-format
msgid "Delete selected entries"
msgstr ""

#: lspclientconfigpage.cpp:280
#, kde-format
msgid "Delete all entries"
msgstr ""

#: lspclientplugin.cpp:224
#, kde-format
msgid "LSP server start requested"
msgstr ""

#: lspclientplugin.cpp:227
#, kde-format
msgid ""
"Do you want the LSP server to be started?<br><br>The full command line is:"
"<br><br><b>%1</b><br><br>The choice can be altered via the config page of "
"the plugin."
msgstr ""

#: lspclientplugin.cpp:240
#, kde-format
msgid ""
"User permanently blocked start of: '%1'.\n"
"Use the config page of the plugin to undo this block."
msgstr ""

#: lspclientpluginview.cpp:450 lspclientpluginview.cpp:704
#, kde-format
msgid "LSP"
msgstr ""

#: lspclientpluginview.cpp:512
#, kde-format
msgid "Go to Definition"
msgstr ""

#: lspclientpluginview.cpp:514
#, kde-format
msgid "Go to Declaration"
msgstr ""

#: lspclientpluginview.cpp:516
#, kde-format
msgid "Go to Type Definition"
msgstr ""

#: lspclientpluginview.cpp:518
#, kde-format
msgid "Find References"
msgstr ""

#: lspclientpluginview.cpp:521
#, kde-format
msgid "Find Implementations"
msgstr ""

#: lspclientpluginview.cpp:523
#, kde-format
msgid "Highlight"
msgstr ""

#: lspclientpluginview.cpp:525
#, kde-format
msgid "Symbol Info"
msgstr ""

#: lspclientpluginview.cpp:527
#, kde-format
msgid "Search and Go to Symbol"
msgstr ""

#: lspclientpluginview.cpp:532
#, kde-format
msgid "Format"
msgstr ""

#: lspclientpluginview.cpp:535
#, kde-format
msgid "Rename"
msgstr ""

#: lspclientpluginview.cpp:539
#, kde-format
msgid "Expand Selection"
msgstr ""

#: lspclientpluginview.cpp:542
#, kde-format
msgid "Shrink Selection"
msgstr ""

#: lspclientpluginview.cpp:546
#, kde-format
msgid "Switch Source Header"
msgstr ""

#: lspclientpluginview.cpp:549
#, kde-format
msgid "Expand Macro"
msgstr ""

#: lspclientpluginview.cpp:551
#, kde-format
msgid "Code Action"
msgstr ""

#: lspclientpluginview.cpp:566
#, kde-format
msgid "Show selected completion documentation"
msgstr ""

#: lspclientpluginview.cpp:569
#, kde-format
msgid "Enable signature help with auto completion"
msgstr ""

#: lspclientpluginview.cpp:572
#, kde-format
msgid "Include declaration in references"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkComplParens)
#: lspclientpluginview.cpp:575 lspconfigwidget.ui:96
#, kde-format
msgid "Add parentheses upon function completion"
msgstr ""

#: lspclientpluginview.cpp:578
#, kde-format
msgid "Show hover information"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkOnTypeFormatting)
#: lspclientpluginview.cpp:581 lspconfigwidget.ui:47
#, kde-format
msgid "Format on typing"
msgstr ""

#: lspclientpluginview.cpp:584
#, kde-format
msgid "Incremental document synchronization"
msgstr ""

#: lspclientpluginview.cpp:587
#, kde-format
msgid "Highlight goto location"
msgstr ""

#: lspclientpluginview.cpp:596
#, kde-format
msgid "Show Inlay Hints"
msgstr ""

#: lspclientpluginview.cpp:600
#, kde-format
msgid "Show Diagnostics Notifications"
msgstr ""

#: lspclientpluginview.cpp:605
#, kde-format
msgid "Show Messages"
msgstr ""

#: lspclientpluginview.cpp:610
#, kde-format
msgid "Server Memory Usage"
msgstr ""

#: lspclientpluginview.cpp:614
#, kde-format
msgid "Close All Dynamic Reference Tabs"
msgstr ""

#: lspclientpluginview.cpp:616
#, kde-format
msgid "Restart LSP Server"
msgstr ""

#: lspclientpluginview.cpp:618
#, kde-format
msgid "Restart All LSP Servers"
msgstr ""

#: lspclientpluginview.cpp:629
#, kde-format
msgid "Go To"
msgstr ""

#: lspclientpluginview.cpp:656
#, kde-format
msgid "More options"
msgstr ""

#: lspclientpluginview.cpp:872 lspclientsymbolview.cpp:287
#, kde-format
msgid "Expand All"
msgstr ""

#: lspclientpluginview.cpp:873 lspclientsymbolview.cpp:288
#, kde-format
msgid "Collapse All"
msgstr ""

#: lspclientpluginview.cpp:1103
#, kde-format
msgid "RangeHighLight"
msgstr ""

#: lspclientpluginview.cpp:1437
#, kde-format
msgid "Line: %1: "
msgstr ""

#: lspclientpluginview.cpp:1589 lspclientpluginview.cpp:1945
#: lspclientpluginview.cpp:2065
#, kde-format
msgid "No results"
msgstr ""

#: lspclientpluginview.cpp:1648
#, kde-format
msgctxt "@title:tab"
msgid "Definition: %1"
msgstr ""

#: lspclientpluginview.cpp:1654
#, kde-format
msgctxt "@title:tab"
msgid "Declaration: %1"
msgstr ""

#: lspclientpluginview.cpp:1660
#, kde-format
msgctxt "@title:tab"
msgid "Type Definition: %1"
msgstr ""

#: lspclientpluginview.cpp:1666
#, kde-format
msgctxt "@title:tab"
msgid "References: %1"
msgstr ""

#: lspclientpluginview.cpp:1678
#, kde-format
msgctxt "@title:tab"
msgid "Implementation: %1"
msgstr ""

#: lspclientpluginview.cpp:1691
#, kde-format
msgctxt "@title:tab"
msgid "Highlight: %1"
msgstr ""

#: lspclientpluginview.cpp:1715 lspclientpluginview.cpp:1726
#: lspclientpluginview.cpp:1739
#, kde-format
msgid "No Actions"
msgstr ""

#: lspclientpluginview.cpp:1730
#, kde-format
msgid "Loading..."
msgstr ""

#: lspclientpluginview.cpp:1822
#, kde-format
msgid "No edits"
msgstr ""

#: lspclientpluginview.cpp:1903
#, kde-format
msgctxt "@title:window"
msgid "Rename"
msgstr ""

#: lspclientpluginview.cpp:1904
#, kde-format
msgctxt "@label:textbox"
msgid "New name (caution: not all references may be replaced)"
msgstr ""

#: lspclientpluginview.cpp:1951
#, kde-format
msgid "Not enough results"
msgstr ""

#: lspclientpluginview.cpp:2021
#, kde-format
msgid "Corresponding Header/Source not found"
msgstr ""

#: lspclientpluginview.cpp:2217 lspclientpluginview.cpp:2254
#, kde-format
msgctxt "@info"
msgid "LSP Server"
msgstr ""

#: lspclientpluginview.cpp:2277
#, kde-format
msgctxt "@info"
msgid "LSP Client"
msgstr ""

#: lspclientpluginview.cpp:2562
#, kde-format
msgid "Question from LSP server"
msgstr ""

#: lspclientservermanager.cpp:652
#, kde-format
msgid "Restarting"
msgstr ""

#: lspclientservermanager.cpp:652
#, kde-format
msgid "NOT Restarting"
msgstr ""

#: lspclientservermanager.cpp:653
#, kde-format
msgid "Server terminated unexpectedly ... %1 [%2] [homepage: %3] "
msgstr ""

#: lspclientservermanager.cpp:861
#, kde-format
msgid "Failed to find server binary: %1"
msgstr ""

#: lspclientservermanager.cpp:864 lspclientservermanager.cpp:906
#, kde-format
msgid "Please check your PATH for the binary"
msgstr ""

#: lspclientservermanager.cpp:865 lspclientservermanager.cpp:907
#, kde-format
msgid "See also %1 for installation or details"
msgstr ""

#: lspclientservermanager.cpp:903
#, kde-format
msgid "Failed to start server: %1"
msgstr ""

#: lspclientservermanager.cpp:911
#, kde-format
msgid "Started server %2: %1"
msgstr ""

#: lspclientservermanager.cpp:946
#, kde-format
msgid "Failed to parse server configuration '%1': no JSON object"
msgstr ""

#: lspclientservermanager.cpp:949
#, kde-format
msgid "Failed to parse server configuration '%1': %2"
msgstr ""

#: lspclientservermanager.cpp:953
#, kde-format
msgid "Failed to read server configuration: %1"
msgstr ""

#: lspclientsymbolview.cpp:240
#, kde-format
msgid "Symbol Outline"
msgstr ""

#: lspclientsymbolview.cpp:278
#, kde-format
msgid "Tree Mode"
msgstr ""

#: lspclientsymbolview.cpp:280
#, kde-format
msgid "Automatically Expand Tree"
msgstr ""

#: lspclientsymbolview.cpp:282
#, kde-format
msgid "Sort Alphabetically"
msgstr ""

#: lspclientsymbolview.cpp:284
#, kde-format
msgid "Show Details"
msgstr ""

#: lspclientsymbolview.cpp:445
#, kde-format
msgid "Symbols"
msgstr ""

#: lspclientsymbolview.cpp:576
#, kde-format
msgid "No LSP server for this document."
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: lspconfigwidget.ui:33
#, kde-format
msgid "Client Settings"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkFmtOnSave)
#: lspconfigwidget.ui:54
#, kde-format
msgid "Format on save"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkSemanticHighlighting)
#: lspconfigwidget.ui:61
#, kde-format
msgid "Enable semantic highlighting"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkInlayHint)
#: lspconfigwidget.ui:68
#, kde-format
msgid "Enable inlay hints"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: lspconfigwidget.ui:75
#, kde-format
msgid "Completions:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkComplDoc)
#: lspconfigwidget.ui:82
#, kde-format
msgid "Show inline docs for selected completion"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkSignatureHelp)
#: lspconfigwidget.ui:89
#, kde-format
msgid "Show function signature when typing a function call"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoImport)
#: lspconfigwidget.ui:103
#, kde-format
msgid "Add imports automatically if needed upon completion"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: lspconfigwidget.ui:110
#, kde-format
msgid "Navigation:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkRefDeclaration)
#: lspconfigwidget.ui:117
#, kde-format
msgid "Count declarations when searching for references to a symbol"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoHover)
#: lspconfigwidget.ui:124
#, kde-format
msgid "Show information about currently hovered symbol"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkHighlightGoto)
#: lspconfigwidget.ui:131
#, kde-format
msgid "Highlight target line when hopping to it"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: lspconfigwidget.ui:138
#, kde-format
msgid "Server:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkDiagnostics)
#: lspconfigwidget.ui:145
#, kde-format
msgid "Show program diagnostics"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkMessages)
#: lspconfigwidget.ui:152
#, kde-format
msgid "Show notifications from the LSP server"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkIncrementalSync)
#: lspconfigwidget.ui:159
#, kde-format
msgid "Incrementally synchronize documents with the LSP server"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: lspconfigwidget.ui:166
#, kde-format
msgid "Document outline:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolSort)
#: lspconfigwidget.ui:173
#, kde-format
msgid "Sort symbols alphabetically"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolDetails)
#: lspconfigwidget.ui:180
#, kde-format
msgid "Display additional details for symbols"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolTree)
#: lspconfigwidget.ui:187
#, kde-format
msgid "Present symbols in a hierarchy instead of a flat list"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolExpand)
#: lspconfigwidget.ui:202
#, kde-format
msgid "Automatically expand tree"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab_4)
#: lspconfigwidget.ui:227
#, kde-format
msgid "Allowed && Blocked Servers"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: lspconfigwidget.ui:237
#, kde-format
msgid "User Server Settings"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label)
#: lspconfigwidget.ui:245
#, kde-format
msgid "Settings File:"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab_3)
#: lspconfigwidget.ui:272
#, kde-format
msgid "Default Server Settings"
msgstr ""

#. i18n: ectx: Menu (lspclient_more_options)
#: ui.rc:31
#, kde-format
msgid "More Options"
msgstr ""
