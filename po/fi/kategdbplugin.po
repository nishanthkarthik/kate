# Finnish messages for kategdbplugin.
# Copyright © 2011, 2012 This_file_is_part_of_KDE
# This file is distributed under the same license as the kdebase package.
# Jorma Karvonen <karvonen.jorma@gmail.com>, 2011-2012.
# Lasse Liehu <lasse.liehu@gmail.com>, 2012, 2013, 2014, 2015, 2016.
# Tommi Nieminen <translator@legisign.org>, 2020, 2022, 2023.
#
# KDE Finnish translation sprint participants:
# Author: Lliehu
# Author: Niklas Laxström
msgid ""
msgstr ""
"Project-Id-Version: kategdbplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-15 00:49+0000\n"
"PO-Revision-Date: 2023-05-29 21:11+0300\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-POT-Import-Date: 2012-12-01 22:21:57+0000\n"
"X-Generator: Lokalize 23.04.1\n"

#. i18n: ectx: property (text), widget (QLabel, u_gdbLabel)
#: advanced_settings.ui:17
#, kde-format
msgid "GDB command"
msgstr "Komento"

#. i18n: ectx: property (text), widget (QToolButton, u_gdbBrowse)
#. i18n: ectx: property (text), widget (QToolButton, u_addSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_setSoPrefix)
#. i18n: ectx: property (text), widget (QToolButton, u_addSoSearchPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSoSearchPath)
#: advanced_settings.ui:30 advanced_settings.ui:62 advanced_settings.ui:69
#: advanced_settings.ui:241 advanced_settings.ui:274 advanced_settings.ui:281
#, kde-format
msgid "..."
msgstr "…"

#. i18n: ectx: property (text), widget (QLabel, u_srcPathsLabel)
#: advanced_settings.ui:37
#, kde-format
msgid "Source file search paths"
msgstr "Lähdetiedostojen hakupolut"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:92
#, kde-format
msgid "Local application"
msgstr "Paikallinen sovellus"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:97
#, kde-format
msgid "Remote TCP"
msgstr "Etä-TCP"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:102
#, kde-format
msgid "Remote Serial Port"
msgstr "Etäsarjaportti"

#. i18n: ectx: property (text), widget (QLabel, u_hostLabel)
#: advanced_settings.ui:127
#, kde-format
msgid "Host"
msgstr "Verkkonimi"

#. i18n: ectx: property (text), widget (QLabel, u_tcpPortLabel)
#. i18n: ectx: property (text), widget (QLabel, u_ttyLabel)
#: advanced_settings.ui:141 advanced_settings.ui:166
#, kde-format
msgid "Port"
msgstr "Portti"

#. i18n: ectx: property (text), widget (QLabel, u_ttyBaudLabel)
#: advanced_settings.ui:183
#, kde-format
msgid "Baud"
msgstr "Baudi"

#. i18n: ectx: property (text), widget (QLabel, u_soAbsPrefixLabel)
#: advanced_settings.ui:231
#, kde-format
msgid "solib-absolute-prefix"
msgstr "solib-absoluuttinen-etuliite"

#. i18n: ectx: property (text), widget (QLabel, u_soSearchLabel)
#: advanced_settings.ui:248
#, kde-format
msgid "solib-search-path"
msgstr "solib-hakupolku"

#. i18n: ectx: property (title), widget (QGroupBox, u_customInitGB)
#: advanced_settings.ui:317
#, kde-format
msgid "Custom Init Commands"
msgstr "Mukautetut alustuskomennot"

#: backend.cpp:24 backend.cpp:49 debugview_dap.cpp:155
#, kde-format
msgid ""
"A debugging session is on course. Please, use re-run or stop the current "
"session."
msgstr ""
"Virheenpaikannusistunto on käynnissä. Suorita uudelleen tai pysäytä nykyinen "
"istunto."

#: configview.cpp:92
#, kde-format
msgid "Add new target"
msgstr "Lisää uusi kohde"

#: configview.cpp:96
#, kde-format
msgid "Copy target"
msgstr "Kopioi kohde"

#: configview.cpp:100
#, kde-format
msgid "Delete target"
msgstr "Poista kohde"

#: configview.cpp:105
#, kde-format
msgid "Executable:"
msgstr "Ohjelmatiedosto:"

#: configview.cpp:125
#, kde-format
msgid "Working Directory:"
msgstr "Työhakemisto:"

#: configview.cpp:133
#, kde-format
msgid "Process Id:"
msgstr "Prosessitunniste:"

#: configview.cpp:138
#, kde-format
msgctxt "Program argument list"
msgid "Arguments:"
msgstr "Argumentit:"

#: configview.cpp:141
#, kde-format
msgctxt "Checkbox to for keeping focus on the command line"
msgid "Keep focus"
msgstr "Pidä kohdistus"

#: configview.cpp:142
#, kde-format
msgid "Keep the focus on the command line"
msgstr "Pidä kohdistus komentorivillä"

#: configview.cpp:144
#, kde-format
msgid "Redirect IO"
msgstr "Uudelleen ohjattu siirräntä"

#: configview.cpp:145
#, kde-format
msgid "Redirect the debugged programs IO to a separate tab"
msgstr ""
"Ohjaa virheenpaikannuksessa olevien ohjelmien siirräntä uudelleen erilliseen "
"välilehteen"

#: configview.cpp:147
#, kde-format
msgid "Advanced Settings"
msgstr "Lisäasetukset"

#: configview.cpp:231
#, kde-format
msgid "Targets"
msgstr "Kohteet"

#: configview.cpp:524 configview.cpp:537
#, kde-format
msgid "Target %1"
msgstr "Kohde %1"

#. i18n: ectx: attribute (title), widget (QWidget, tab_1)
#: debugconfig.ui:33
#, kde-format
msgid "User Debug Adapter Settings"
msgstr "Virheenpaikannussovittimen käyttäjäasetukset"

#. i18n: ectx: property (text), widget (QLabel, label)
#: debugconfig.ui:41
#, kde-format
msgid "Settings File:"
msgstr "Asetustiedosto:"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: debugconfig.ui:68
#, kde-format
msgid "Default Debug Adapter Settings"
msgstr "Virheenpaikannussovittimen oletusasetukset"

#: debugconfigpage.cpp:72 debugconfigpage.cpp:77
#, kde-format
msgid "Debugger"
msgstr "Virheenpaikannin"

#: debugconfigpage.cpp:128
#, kde-format
msgid "No JSON data to validate."
msgstr "Ei validoitavaa JSON-dataa."

#: debugconfigpage.cpp:136
#, kde-format
msgid "JSON data is valid."
msgstr "JSON-data on kelvollista."

#: debugconfigpage.cpp:138
#, kde-format
msgid "JSON data is invalid: no JSON object"
msgstr "JSON-data on kelvotonta: ei JSON-oliota"

#: debugconfigpage.cpp:141
#, kde-format
msgid "JSON data is invalid: %1"
msgstr "JSON-data on kelvotonta: %1"

#: debugview.cpp:35
#, kde-format
msgid "Locals"
msgstr "Paikalliset"

#: debugview.cpp:37
#, kde-format
msgid "CPU registers"
msgstr "Suoritinrekisterit"

#: debugview.cpp:160
#, kde-format
msgid "Please set the executable in the 'Settings' tab in the 'Debug' panel."
msgstr ""
"Aseta ohjelmatiedosto Virheenpaikannus-paneelin Asetukset-välilehdellä."

#: debugview.cpp:169
#, kde-format
msgid ""
"No debugger selected. Please select one in the 'Settings' tab in the 'Debug' "
"panel."
msgstr ""
"Virheenjäljitintä ei ole valittu. Valitse se Virheenpaikannus-paneelin "
"Asetukset-välilehdeltä."

#: debugview.cpp:178
#, kde-format
msgid ""
"Debugger not found. Please make sure you have it installed in your system. "
"The selected debugger is '%1'"
msgstr ""
"Virheenjäljitintä ei löydy. Varmista, että se on järjestelmään asennettu. "
"Valittu vianjäljitin on ”%1”"

#: debugview.cpp:384
#, kde-format
msgid "Could not start debugger process"
msgstr "Ei voitu käynnistää virheenpaikanninprosessia"

#: debugview.cpp:442
#, kde-format
msgid "*** gdb exited normally ***"
msgstr "*** gdb päättyi normaalisti ***"

#: debugview.cpp:648
#, kde-format
msgid "all threads running"
msgstr "kaikki säikeet käynnissä"

#: debugview.cpp:650
#, kde-format
msgid "thread(s) running: %1"
msgstr "säikeitä käynnissä: %1"

#: debugview.cpp:655 debugview_dap.cpp:270
#, kde-format
msgid "stopped (%1)."
msgstr "pysäytetty (%1)."

#: debugview.cpp:659 debugview_dap.cpp:278
#, kde-format
msgid "Active thread: %1 (all threads stopped)."
msgstr "Aktiivinen säie: %1 (kaikki säikeet pysäytetty)."

#: debugview.cpp:661 debugview_dap.cpp:280
#, kde-format
msgid "Active thread: %1."
msgstr "Aktiivinen säie: %1."

#: debugview.cpp:680
#, kde-format
msgid "Current frame: %1:%2"
msgstr "Nykyinen kehys: %1:%2"

#: debugview.cpp:707
#, kde-format
msgid "Host: %1. Target: %1"
msgstr "Kone: %1. Kohde: %1"

#: debugview.cpp:1377
#, kde-format
msgid ""
"gdb-mi: Could not parse last response: %1. Too many consecutive errors. "
"Shutting down."
msgstr ""
"gdb-mi: Ei voitu jäsentää viimeisintä vastausta: %1. Liian monta peräkkäistä "
"virhettä. Lopetetaan."

#: debugview.cpp:1379
#, kde-format
msgid "gdb-mi: Could not parse last response: %1"
msgstr "gdb-mi: Ei voitu jäsentää viimeisintä vastausta: %1"

#: debugview_dap.cpp:169
#, kde-format
msgid "DAP backend failed"
msgstr "DAP-taustajärjestelmä epäonnistui"

#: debugview_dap.cpp:211
#, kde-format
msgid "program terminated"
msgstr "ohjelma päätettiin"

#: debugview_dap.cpp:223
#, kde-format
msgid "requesting disconnection"
msgstr "pyydetään yhteyden katkaisua"

#: debugview_dap.cpp:237
#, kde-format
msgid "requesting shutdown"
msgstr "pyydetään sammutusta"

#: debugview_dap.cpp:261
#, kde-format
msgid "DAP backend: %1"
msgstr "DAP-taustajärjestelmä: %1"

#: debugview_dap.cpp:285
#, kde-format
msgid "Breakpoint(s) reached:"
msgstr "Keskeytyskohdat saavutettu:"

#: debugview_dap.cpp:307
#, kde-format
msgid "(continued) thread %1"
msgstr "(jatkettiin) säie %1"

#: debugview_dap.cpp:309
#, kde-format
msgid "all threads continued"
msgstr "jatkettiin kaikkia säikeitä"

#: debugview_dap.cpp:316
#, kde-format
msgid "(running)"
msgstr "(käynnissä)"

#: debugview_dap.cpp:404
#, kde-format
msgid "*** connection with server closed ***"
msgstr "*** palvelinyhteys sulkeutui ***"

#: debugview_dap.cpp:411
#, kde-format
msgid "program exited with code %1"
msgstr "ohjelma päättyi koodilla %1"

#: debugview_dap.cpp:425
#, kde-format
msgid "*** waiting for user actions ***"
msgstr "*** odotetaan käyttäjän toimia ***"

#: debugview_dap.cpp:430
#, kde-format
msgid "error on response: %1"
msgstr "virhe vastauksessa: %1"

#: debugview_dap.cpp:445
#, kde-format
msgid "important"
msgstr "tärkeä"

#: debugview_dap.cpp:448
#, kde-format
msgid "telemetry"
msgstr "telemetria"

#: debugview_dap.cpp:467
#, kde-format
msgid "debugging process [%1] %2"
msgstr "virheenpaikannusprosessi [%1] %2"

#: debugview_dap.cpp:469
#, kde-format
msgid "debugging process %1"
msgstr "virheenpaikannusprosessi %1"

#: debugview_dap.cpp:472
#, kde-format
msgid "Start method: %1"
msgstr "Aloitustapa: %1"

#: debugview_dap.cpp:479
#, kde-format
msgid "thread %1"
msgstr "säie %1"

#: debugview_dap.cpp:633
#, kde-format
msgid "breakpoint set"
msgstr "keskeytyskohta asetettu"

#: debugview_dap.cpp:641
#, kde-format
msgid "breakpoint cleared"
msgstr "keskeytyskohta poistettu"

#: debugview_dap.cpp:700
#, kde-format
msgid "(%1) breakpoint"
msgstr "(%1) keskeytyskohta"

#: debugview_dap.cpp:717
#, kde-format
msgid "<not evaluated>"
msgstr "<ei suoritettu>"

#: debugview_dap.cpp:739
#, kde-format
msgid "server capabilities"
msgstr "palvelimen ominaisuudet"

#: debugview_dap.cpp:742
#, kde-format
msgid "supported"
msgstr "tuettu"

#: debugview_dap.cpp:742
#, kde-format
msgid "unsupported"
msgstr "ei tuettu"

#: debugview_dap.cpp:745
#, kde-format
msgid "conditional breakpoints"
msgstr "ehdolliset keskeytyskohdat"

#: debugview_dap.cpp:746
#, kde-format
msgid "function breakpoints"
msgstr "funktion keskeytyskohdat"

#: debugview_dap.cpp:747
#, kde-format
msgid "hit conditional breakpoints"
msgstr ""

#: debugview_dap.cpp:748
#, kde-format
msgid "log points"
msgstr "lokipisteet"

#: debugview_dap.cpp:748
#, kde-format
msgid "modules request"
msgstr "moduulipyyntö"

#: debugview_dap.cpp:749
#, kde-format
msgid "goto targets request"
msgstr "goto-määränpääpyyntö"

#: debugview_dap.cpp:750
#, kde-format
msgid "terminate request"
msgstr "lopeta pyyntö"

#: debugview_dap.cpp:751
#, kde-format
msgid "terminate debuggee"
msgstr "lopeta jäljitettävä"

#: debugview_dap.cpp:958
#, kde-format
msgid "syntax error: expression not found"
msgstr "syntaksivirhe: lauseketta ei löytynyt"

#: debugview_dap.cpp:976 debugview_dap.cpp:1011 debugview_dap.cpp:1049
#: debugview_dap.cpp:1083 debugview_dap.cpp:1119 debugview_dap.cpp:1155
#: debugview_dap.cpp:1191 debugview_dap.cpp:1291 debugview_dap.cpp:1353
#, kde-format
msgid "syntax error: %1"
msgstr "syntaksivirhe: %1"

#: debugview_dap.cpp:984 debugview_dap.cpp:1019 debugview_dap.cpp:1298
#: debugview_dap.cpp:1361
#, kde-format
msgid "invalid line: %1"
msgstr "virheellinen rivi: %1"

#: debugview_dap.cpp:991 debugview_dap.cpp:996 debugview_dap.cpp:1026
#: debugview_dap.cpp:1031 debugview_dap.cpp:1322 debugview_dap.cpp:1327
#: debugview_dap.cpp:1368 debugview_dap.cpp:1373
#, kde-format
msgid "file not specified: %1"
msgstr "tiedostoa ei määritetty: %1"

#: debugview_dap.cpp:1061 debugview_dap.cpp:1095 debugview_dap.cpp:1131
#: debugview_dap.cpp:1167 debugview_dap.cpp:1203
#, kde-format
msgid "invalid thread id: %1"
msgstr "virheellinen säikeen tunniste: %1"

#: debugview_dap.cpp:1067 debugview_dap.cpp:1101 debugview_dap.cpp:1137
#: debugview_dap.cpp:1173 debugview_dap.cpp:1209
#, kde-format
msgid "thread id not specified: %1"
msgstr "säikeen tunnistetta ei annettu: %1"

#: debugview_dap.cpp:1220
#, kde-format
msgid "Available commands:"
msgstr "Käytettävissä olevat komennot:"

#: debugview_dap.cpp:1308
#, kde-format
msgid "conditional breakpoints are not supported by the server"
msgstr "palvelin ei tue ehdollisia keskeytyskohtia"

#: debugview_dap.cpp:1316
#, kde-format
msgid "hit conditional breakpoints are not supported by the server"
msgstr ""

#: debugview_dap.cpp:1336
#, kde-format
msgid "line %1 already has a breakpoint"
msgstr "rivillä %1 on jo keskeytyskohta"

#: debugview_dap.cpp:1381
#, kde-format
msgid "breakpoint not found (%1:%2)"
msgstr "keskeytyskohtaa ei löydy (%1:%2)"

#: debugview_dap.cpp:1387
#, kde-format
msgid "Current thread: "
msgstr "Nykyinen säie:"

#: debugview_dap.cpp:1392 debugview_dap.cpp:1399 debugview_dap.cpp:1423
#, kde-format
msgid "none"
msgstr "ei mitään"

#: debugview_dap.cpp:1395
#, kde-format
msgid "Current frame: "
msgstr "Nykyinen kehys:"

#: debugview_dap.cpp:1402
#, kde-format
msgid "Session state: "
msgstr "Istunnon tila:"

#: debugview_dap.cpp:1405
#, kde-format
msgid "initializing"
msgstr "Alustetaan"

#: debugview_dap.cpp:1408
#, kde-format
msgid "running"
msgstr "käynnissä"

#: debugview_dap.cpp:1411
#, kde-format
msgid "stopped"
msgstr "pysäytetty"

#: debugview_dap.cpp:1414
#, kde-format
msgid "terminated"
msgstr "päätetty"

#: debugview_dap.cpp:1417
#, kde-format
msgid "disconnected"
msgstr "yhteys katkaistu"

#: debugview_dap.cpp:1420
#, kde-format
msgid "post mortem"
msgstr "ruumiinavaus"

#: debugview_dap.cpp:1476
#, kde-format
msgid "command not found"
msgstr "komentoa ei löydy"

#: debugview_dap.cpp:1497
#, kde-format
msgid "missing thread id"
msgstr "puuttuva säietunniste"

#: debugview_dap.cpp:1605
#, kde-format
msgid "killing backend"
msgstr "tapetaan taustajärjestelmää"

#: debugview_dap.cpp:1663
#, kde-format
msgid "Current frame [%3]: %1:%2 (%4)"
msgstr "Nykyinen kehys [%3]: %1:%2 (%4)"

#: localsview.cpp:17
#, kde-format
msgid "Symbol"
msgstr "Symboli"

#: localsview.cpp:18
#, kde-format
msgid "Value"
msgstr "Arvo"

#: localsview.cpp:41
#, kde-format
msgid "type"
msgstr "tyyppi"

#: localsview.cpp:50
#, kde-format
msgid "indexed items"
msgstr "indeksoidut kohteet"

#: localsview.cpp:53
#, kde-format
msgid "named items"
msgstr "nimetyt kohteet"

#: plugin_kategdb.cpp:106
#, kde-format
msgid "Kate Debug"
msgstr "Kate-virheenpaikannin"

#: plugin_kategdb.cpp:110
#, kde-format
msgid "Debug View"
msgstr "Virheenpaikannusnäkymä"

#: plugin_kategdb.cpp:110 plugin_kategdb.cpp:343
#, kde-format
msgid "Debug"
msgstr "Virheenpaikannus"

#: plugin_kategdb.cpp:113 plugin_kategdb.cpp:116
#, kde-format
msgid "Locals and Stack"
msgstr "Paikalliset muuttujat ja pino"

#: plugin_kategdb.cpp:168
#, kde-format
msgctxt "Column label (frame number)"
msgid "Nr"
msgstr "Nro"

#: plugin_kategdb.cpp:168
#, kde-format
msgctxt "Column label"
msgid "Frame"
msgstr "Kehys"

#: plugin_kategdb.cpp:200
#, kde-format
msgctxt "Tab label"
msgid "Debug Output"
msgstr "Virheenpaikannustuloste"

#: plugin_kategdb.cpp:201
#, kde-format
msgctxt "Tab label"
msgid "Settings"
msgstr "Asetukset"

#: plugin_kategdb.cpp:243
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<title>Could not open file:</title><nl/>%1<br/>Try adding a search path to "
"Advanced Settings -> Source file search paths"
msgstr ""
"<title>Tiedostoa ei voitu avata:</title><nl/>%1<br/>Kokeile lisätä hakupolku "
"kohdassa Lisäasetukset → Lähdetiedostojen hakupolut"

#: plugin_kategdb.cpp:268
#, kde-format
msgid "Start Debugging"
msgstr "Aloita virheenpaikannus"

#: plugin_kategdb.cpp:278
#, kde-format
msgid "Kill / Stop Debugging"
msgstr "Tapa / pysäytä virheenpaikannus"

#: plugin_kategdb.cpp:285
#, kde-format
msgid "Continue"
msgstr "Jatka"

#: plugin_kategdb.cpp:291
#, kde-format
msgid "Toggle Breakpoint / Break"
msgstr "Vaihda keskeytyskohtaa / keskeytystä"

#: plugin_kategdb.cpp:297
#, kde-format
msgid "Step In"
msgstr "Astu sisään"

#: plugin_kategdb.cpp:304
#, kde-format
msgid "Step Over"
msgstr "Astu yli"

#: plugin_kategdb.cpp:311
#, kde-format
msgid "Step Out"
msgstr "Astu ulos"

#: plugin_kategdb.cpp:318 plugin_kategdb.cpp:350
#, kde-format
msgid "Run To Cursor"
msgstr "Suorita kohdistimeen"

#: plugin_kategdb.cpp:325
#, kde-format
msgid "Restart Debugging"
msgstr "Käynnistä virheenpaikannus uudelleen"

#: plugin_kategdb.cpp:333 plugin_kategdb.cpp:352
#, kde-format
msgctxt "Move Program Counter (next execution)"
msgid "Move PC"
msgstr "Siirrä ohjelmalaskuri"

#: plugin_kategdb.cpp:338
#, kde-format
msgid "Print Value"
msgstr "Tulosta arvo"

#: plugin_kategdb.cpp:347
#, kde-format
msgid "popup_breakpoint"
msgstr "popup_breakpoint"

#: plugin_kategdb.cpp:349
#, kde-format
msgid "popup_run_to_cursor"
msgstr "popup_run_to_cursor"

#: plugin_kategdb.cpp:431 plugin_kategdb.cpp:447
#, kde-format
msgid "Insert breakpoint"
msgstr "Lisää keskeytyskohta"

#: plugin_kategdb.cpp:445
#, kde-format
msgid "Remove breakpoint"
msgstr "Poista keskeytyskohta"

#: plugin_kategdb.cpp:599 plugin_kategdb.cpp:613
#, kde-format
msgid "Execution point"
msgstr "Suorituspiste"

#: plugin_kategdb.cpp:771
#, kde-format
msgid "Thread %1"
msgstr "Säie %1"

#: plugin_kategdb.cpp:871
#, kde-format
msgid "IO"
msgstr "Siirräntä"

#: plugin_kategdb.cpp:956 plugin_kategdb.cpp:964
#, kde-format
msgid "Breakpoint"
msgstr "Keskeytyskohta"

#. i18n: ectx: Menu (debug)
#: ui.rc:6
#, kde-format
msgid "&Debug"
msgstr "&Virheenpaikannus"

#. i18n: ectx: ToolBar (gdbplugin)
#: ui.rc:29
#, kde-format
msgid "Debug Plugin"
msgstr "Virheenpaikannusliitännäinen"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Tommi Nieminen"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "translator@legisign.org"

#~ msgid "GDB Integration"
#~ msgstr "GDB-integraatio"

#~ msgid "Kate GDB Integration"
#~ msgstr "Kate GDB -integraatio"

#~ msgid "&Target:"
#~ msgstr "&Kohde:"

#~ msgctxt "Program argument list"
#~ msgid "&Arg List:"
#~ msgstr "&Argumenttiluettelo:"

#~ msgid "Remove Argument List"
#~ msgstr "Poista argumenttiluettelo"

#~ msgid "Arg Lists"
#~ msgstr "Argumenttiluettelot"
