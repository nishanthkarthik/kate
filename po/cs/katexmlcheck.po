# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Vít Pelčák <vit@pelcak.org>, 2016, 2017, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-11 00:50+0000\n"
"PO-Revision-Date: 2023-01-17 16:11+0100\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 22.12.1\n"

#: plugin_katexmlcheck.cpp:95
#, kde-format
msgid "XML Check"
msgstr "Kontrola XML"

#: plugin_katexmlcheck.cpp:100
#, kde-format
msgid "Validate XML"
msgstr "Ověřit XML"

#: plugin_katexmlcheck.cpp:130
#, kde-format
msgid "Validate process crashed"
msgstr "Ověřovací proces zhavaroval"

#: plugin_katexmlcheck.cpp:130 plugin_katexmlcheck.cpp:148
#: plugin_katexmlcheck.cpp:208 plugin_katexmlcheck.cpp:233
#: plugin_katexmlcheck.cpp:253 plugin_katexmlcheck.cpp:341
#, kde-format
msgid "XMLCheck"
msgstr "XMLCheck"

#: plugin_katexmlcheck.cpp:144
#, kde-format
msgid "No DOCTYPE found, will only check well-formedness."
msgstr ""

#: plugin_katexmlcheck.cpp:146
#, kde-format
msgctxt "%1 refers to the XML DTD"
msgid "'%1' not found, will only check well-formedness."
msgstr ""

#: plugin_katexmlcheck.cpp:232
#, kde-format
msgid "<b>Error:</b> Could not create temporary file '%1'."
msgstr "<b>Chyba:</b> nelze vytvořit dočasný soubor '%1'."

#: plugin_katexmlcheck.cpp:251
#, kde-format
msgid ""
"<b>Error:</b> Failed to find xmllint. Please make sure that xmllint is "
"installed. It is part of libxml2."
msgstr ""
"<b>Chyba:</b> Nelze najít 'xmllint'. Prosím ujistěte se, že jej máte "
"nainstalován. Je součástí balíčku libxml2."

#: plugin_katexmlcheck.cpp:339
#, kde-format
msgid ""
"<b>Error:</b> Failed to execute xmllint. Please make sure that xmllint is "
"installed. It is part of libxml2."
msgstr ""
"<b>Chyba:</b> nelze spustit 'xmllint'. Prosím ujistěte se, že jej máte "
"nainstalován (je součástí balíčku 'libxml2'."

#. i18n: ectx: Menu (xml)
#: ui.rc:6
#, kde-format
msgid "&XML"
msgstr "&XML"
